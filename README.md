# MLops homework: Reproducible experiments with DVC

This is a homework repository for the [MLOps & production for data science research 3.0](https://ods.ai/tracks/mlops3-course-spring-2024).

A (very basic) report is deployed at https://homework-05-daiyousei2014-8d7e381e3645459bb7c3f91a47f21d4e8263c.gitlab.io/

I'm not fixin others' bugs for free, no sir.

## Running:

`conda env create -f dev.yaml`

`conda run -n dvc dvc pull` (Keys are required! If you forgot to add them and ran out of compute, tough.)

`conda run -n dvc dvc exp run -n 'experiment_name' -S param_to_change=value`
