import pandas as pd
import dvc.api

from sklearn.model_selection import cross_val_score
from sklearn.linear_model import SGDClassifier
from sklearn.ensemble import HistGradientBoostingClassifier
from joblib import dump, load

classifier_method = dvc.api.params_show().get("classifier_method")

model_map = {
    "sgd": SGDClassifier,
    "gbm": HistGradientBoostingClassifier,
}

model = model_map.get(classifier_method, "sgd")(
    **dvc.api.params_show().get(classifier_method, "sgd")
)
vectorizer = load("model/vectorizer.joblib")

train_data = pd.read_csv("data/train.csv")

X_train = vectorizer.transform(train_data["full_text"]).toarray()
y_train = train_data["label"]

model.fit(X_train, y_train)

dump(model, "model/model.joblib")

cv_logloss = -cross_val_score(
    model, X_train, y_train, cv=5, scoring="neg_log_loss"
)
cv_brier = -cross_val_score(
    model, X_train, y_train, cv=5, scoring="neg_brier_score"
)

dump(cv_logloss, "model/logloss.joblib")
dump(cv_brier, "model/brier.joblib")
