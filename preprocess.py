import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer

vectorizer = CountVectorizer()

train_data = pd.read_csv("data/train.csv")

X = vectorizer.fit_transform(data["text"])

joblib.dump(vectorizer, "vectorizer.joblib")
